# ProVerif Model of the SPTP protocol

## Structure

``` bash
RSP
├── lib
│   ├── rsp_default_smdp_cloning.pvl    # Default SM-DP+ profile initialisation
├── queries
│   ├── cloning.pv                  # Queries for AC and default SMDP
├── Makefile
└── README.md
```

## Running Tests
```
$ make (default)
```
This runs the test when the server is compromised.
