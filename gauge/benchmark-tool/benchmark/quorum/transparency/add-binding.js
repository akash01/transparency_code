/**
* Original work Copyright 2017 HUAWEI. All Rights Reserved.
*
* Modified work Copyright Persistent Systems 2018. All Rights Reserved.
*
* SPDX-License-Identifier: Apache-2.0
*/

'use strict';

const logger = require('../../../src/comm/util');
const cmn = require('./common')
module.exports.info = 'Add Binding';

let bc;
let contx;
let accounts = [];

module.exports.init = (blockchain, context, args) => {
    bc = blockchain;
    contx = context;
    return Promise.resolve();
};

module.exports.run = (iteration) => {
    let newAcc = 'accounts_' + accounts.length;
    accounts.push(newAcc);
    let imsi = iteration.toString();
    let eid = "EID".concat(imsi);
    logger.info('iteration', iteration, imsi, eid)
    let privateIndexInHex = cmn.privateIndex(imsi);
    let commitmentInHex = cmn.calculateCommitment(imsi, eid, 'profile');

    //logger.info('privateIndexInHex', imsi, privateIndexInHex, commitmentInHex);
    return bc.invokeSmartContract(contx, 'BindingRegistry', 'v0', [{verb: 'addBinding'}, {imsiIndex: privateIndexInHex, commitment: commitmentInHex}], 120);
};

module.exports.end = (results) => Promise.resolve();
