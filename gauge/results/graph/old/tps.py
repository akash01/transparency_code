import matplotlib.pyplot as plt

x = [1000, 10000, 100000] # multiple of 1000

# RAFT
y0 = [259, 239, 150] # AB_TPS_average
y1 = [1248, 712, 227] # QB_TPS_average
y2 = [178, 148, 80] # SB_TPS_average
y3 = [1396, 803, 201] # SQB_TPS_average

# IBFT
y4 = [242, 191, 100] # AB_TPS_average
y5 = [1332, 850, 300] # QB_TPS_average
y6 = [158, 148, 51] # SB_TPS_average
y7 = [1334, 641, 300] # SQB_TPS_averag

#plt.plot(x, y0, label="RAFT Add Binding")
plt.plot(x, y0, label="RAFT Add Binding")
plt.plot(x, y1, label="RAFT Query Binding")
plt.plot(x, y2, label="RAFT Sign Binding")
plt.plot(x, y3, label="RAFT Query Sign Binding")

plt.plot(x, y4, label="IBFT Add Binding")
plt.plot(x, y5, label="IBFT Query Binding")
plt.plot(x, y6, label="IBFT Sign Binding")
plt.plot(x, y7, label="IBFT Query Sign Binding")

plt.xlabel('Datum')
plt.ylabel('Seconds')
plt.xscale('log')

plt.title("Average Transaction Per Second")

plt.legend()

plt.show()