import matplotlib.pyplot as plt

x = [1000, 10000, 100000] # multiple of 1000
y0 = [242, 191, 100] # AB_TPS_average
y1 = [1332, 850, 300] # QB_TPS_average
y2 = [158, 148, 51] # SB_TPS_average
y3 = [1334, 641, 300] # SQB_TPS_average

plt.plot(x, y0, label="IBFT Add Binding", marker='o')
plt.plot(x, y1, label="IBFT Query Binding", marker='o')
plt.plot(x, y2, label="IBFT Sign Binding", marker='o')
plt.plot(x, y3, label="IBFT Query Sign Binding", marker='o')

plt.xlabel('Datum')
plt.ylabel('Seconds')
plt.xscale('log')

plt.title("Average Transaction Per Second")

plt.legend()

plt.show()