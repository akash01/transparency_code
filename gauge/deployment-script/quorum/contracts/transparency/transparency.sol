pragma solidity ^0.4.2;

contract BindingRegistry{

   struct Signer{
      string signature;
   }

   struct Record {
      bool initialized;
      bytes32 commitment;
      uint256 versionId; //nonce -- currently a rolling index, can be blockheight
      bytes32 earlierCommitment;
      mapping(bytes32 => Signer) signers;
   }

   mapping(bytes32 => Record) public records;

   event event_bindingChanged(bytes32 index, bytes32 commitnment, string note);

   function initializeIfNeeded(bytes32 index) internal {
      if (!records[index].initialized) {
         records[index].initialized = true;
         records[index].commitment = stringToBytes32('');
         records[index].versionId = 0;
         records[index].earlierCommitment = stringToBytes32('');
      }
   }

   // An SM-DP+ adds a new binding to the SC. SMDP signs own binding
   function addBinding(string imsiIndex, string stringCommitment) returns (bytes32){
      bytes32 index = stringToBytes32(imsiIndex);
      bytes32 nonConfirmedCommitment = stringToBytes32(stringCommitment);

      records[index].earlierCommitment =  records[index].commitment;
      uint newVersionId =  records[index].versionId + 1;
      records[index].versionId = newVersionId;
      bytes32 newCommitment = keccak256(newVersionId, nonConfirmedCommitment);
      records[index].commitment = newCommitment;

      event_bindingChanged(index, newCommitment, "binding has changed");
      return (index);
   }

   // An attester attests a binding
   function addSign(string imsiIndex, string stringSignerAddress, string stringSignature) returns (bytes32){
      bytes32 index = stringToBytes32(imsiIndex);
      bytes32 signerAddress = stringToBytes32(stringSignerAddress);
      records[index].signers[signerAddress].signature = stringSignature;
      return (index);
   }

   // Takes index and returns the current commitment. The return type is
   // bytes32. It is actually a hash value. This is read by Attester
   function lookupBindingCommitment(string imsiIndex) public constant returns(bytes32, bytes32, uint ){
      bytes32 index = stringToBytes32(imsiIndex);
      return (records[index].commitment, records[index].earlierCommitment, records[index].versionId);
   }

   //Reads the current attestation for a commitment by a signer. Performed by
   // Monitors or SM-DP to sent proof of attestation
   function lookupBindingSignature(string imsiIndex, string stringSignerAddress) public constant returns(string) {
      bytes32 index = stringToBytes32(imsiIndex);
      bytes32 signerAddress = stringToBytes32(stringSignerAddress);
      return (records[index].signers[signerAddress].signature);
   }


   //Library functions
   function stringToBytes32(string memory source) returns (bytes32 result) {
      bytes memory tempEmptyStringTest = bytes(source);
      if (tempEmptyStringTest.length == 0) {
         return 0x0;
      }

      assembly {
         result := mload(add(source, 32))
      }
   }

   function bytes32ToString(bytes32 x) constant returns (string) {
      bytes memory bytesString = new bytes(32);
      uint charCount = 0;
      for (uint j = 0; j < 32; j++) {
         byte char = byte(bytes32(uint(x) * 2 ** (8 * j)));
         if (char != 0) {
            bytesString[charCount] = char;
            charCount++;
         }
      }
      bytes memory bytesStringTrimmed = new bytes(charCount);
      for (j = 0; j < charCount; j++) {
         bytesStringTrimmed[j] = bytesString[j];
      }
      return string(bytesStringTrimmed);
   }

}