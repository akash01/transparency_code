# Experiment on Transparency of SIM profiles for the Consumer Remote SIM Provisioning protocol

Directory structure:

* proverif - formal model of SPTP using ProVerif
* code - smart contract for the transparency ledger
* tests - tests used for the experiment
* results - results from the experiment
* quorum-example - quorum network fork used for experiment
* gauge - gauge fork

Gauge tool is a performance benchmarking tool built for characterizing the performance of Hyperledger Fabric platform(v1.0 - v1.4) and Quorum v2.0.0. The benchmarking engine is forked off Caliper (currently Hyperledger Caliper), developed by Huawei Technologies. Tool includes modifications to the Caliper Benchmarking engine to be able to send transactions at high send rates. Modifications to the original Caliper source code are listed here.

This tool comprises of the following tests:

Controlled workloads that measure the throughput and latency of the blockchain.
Micro-benchmarks that tune transaction and chaincode parameters to study its effect on transaction latency.
Scaling tests that study the impact of scaling chaincodes, channels and peers in the network, on system throughput and latency.
Supported Performance metrics:

Throughput (TPS) - The number of transactions that are successfully committed by the blockchain per second.
Transaction confirmation latency - The round trip time from the client submitting a transaction to the time the client receives an event confirmation from the peer.
Resource consumption on peers (CPU and memory) - Amount of memory and CPU utilized on the peers.

PS: **This fork has been [modified](#gauge-modifications) to run this experiment**.

Fork of [gauge](https://github.com/persistentsystems/gauge.git)

PS: **Seems [gauge](https://github.com/persistentsystems/gauge.git) has been removed the repo from github. Therefore, we have included the version we forked for the experiment in this repo.**

PS: We have submitted a paper to a conference based on the findings of this paper.

## Prerequesites

* Docker
* Docker Compose
* Update docker config to support monitoring

```sh
sudo systemctl edit docker.service
[Service]
ExecStart=
ExecStart=/usr/bin/dockerd -H fd:// -H tcp://127.0.0.1:2375

sudo systemctl daemon-reload
sudo systemctl restart docker.service
sudo netstat -lntp | grep dockerd

ps aux|grep dockerd
```

## Quickstart

```sh
# Clone repo
git clone <url/transparency_code.git> $HOME/transparency_code
mv $HOME/transparency_code/quorum_prototype/gauge $HOME
mv $HOME/transparency_code/quorum_prototype/quorum-examples $HOME
cd $HOME/gauge

# Start quorum network
- Ensure gauge/benchmark-tool/benchmark/quorum/quorum.json has <quorum-network-ip> or localhost
make networks # ibft consensus
make networks con=raft # raft consensu

# Start kafka  (Message Queue)
- Ensure gauge/benchmark-tool/listener/kafka-config.json has <kafka-vm-ip> or localhost
make kafka

# Install gauge npm dependencies
make install

# Deploy contract to network
make deploy

# Run experiment
- Ensure gauge/benchmark-tool/benchmark/quorum/transparency/<config>.json has <quorum-node-vm-ip> or localhost
- Update **txNumbAndTps** gauge/benchmark-tool/benchmark/quorum/transparency/binding.json
- Update gauge/benchmark-tool/benchmark/quorum/transparency/signature.json
./run.sh

```

## Manual setup

```sh
git clone https://github.com/jpmorganchase/quorum-examples
cd quorum-examples
Ensure docker compose is latest to support 3..
docker-compose up -d

git clone https://github.com/persistentsystems/gauge.git
vim benchmark-tool/listener/kafka-config.json
Add vm public ip address to kafka and zookeper
Update gauge/kafka-setup/docker-compose-kafka.yml: KAFKA_ADVERTISED_HOST_NAME: <public-ip>
docker-compose up -f kafka up -d

cd gauge/benchmark-tool
npm install
vim benchmark-tool/benchmark/quorum/write/config-quorum-open.json
"docker": {
    "name": [
        "http://localhost:2375/quorum-examples_node1_1",
        "http://localhost:2375/quorum-examples_node2_1",
        "http://localhost:2375/quorum-examples_node3_1"
    ]
},

                CURRENT_NODE=node1 npm run quorum-open
# ad configs

# deploy your contract
cd /home/ubuntu/gauge/deployment-script/quorum
CURRENT_NODE="node1" node deploy.js quorum.json
# Update deployment-script/quorum/quorum.json with
{
        "name": "BindingRegistry",
        "experimentName": "BindingRegistry",
        "path": "quorum/contracts/transparency/transparency.sol",
}
CURRENT_NODE="node1" node deploy.js quorum.json


In case of quorum node restart:
docker prune volume -f
```

## Gauge Modifications

* Add support for 3 parameters: <https://bitbucket.org/shohel/idpaper/src/a66d19e7647ad70d188d4ee20d90a25f9a2c34b6/quorum_prototype/gauge/benchmark-tool/src/quorum/e2eUtils.js#lines-148>
* Add support for multiple parameters: <https://bitbucket.org/shohel/idpaper/src/a66d19e7647ad70d188d4ee20d90a25f9a2c34b6/quorum_prototype/gauge/benchmark-tool/src/quorum/e2eUtils.js#lines-196>
* Add Makefile, run-exp.sh and utils to easily re-run experiments

## Source

* <https://docs.docker.com/install/linux/linux-postinstall/#configure-where-the-docker-daemon-listens-for-connections>
* <https://github.com/persistentsystems/gauge>
* <https://www.researchgate.net/publication/327570196_Performance_Evaluation_of_the_Quorum_Blockchain_Platform>
