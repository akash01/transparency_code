#!/bin/bash
BWhite='\033[1;37m'

docker-compose down
docker volume prune -f

con=$1
if [ ${con} == "raft" ]; then
    echo -e "${BWhite}RAFT consensus"
    QUORUM_CONSENSUS=raft docker-compose up -d
else
    echo -e "${BWhite}IBFT consensus"
    docker-compose up -d
fi

